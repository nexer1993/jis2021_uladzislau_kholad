package by.epam.gomel.training.kholad.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LanguageController {
    @GetMapping("/language")
    public String registration(Model model) {
        return "language";
    }
}
