package by.epam.gomel.training.kholad.repository;

import by.epam.gomel.training.kholad.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
