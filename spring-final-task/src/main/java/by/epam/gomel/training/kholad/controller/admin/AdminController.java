package by.epam.gomel.training.kholad.controller.admin;

import by.epam.gomel.training.kholad.entity.User;
import by.epam.gomel.training.kholad.service.CommentTaskService;
import by.epam.gomel.training.kholad.service.ProjectService;
import by.epam.gomel.training.kholad.service.TaskService;
import by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private CommentTaskService commentTaskService;

    @GetMapping("/admin")
    public String index(Model model,@AuthenticationPrincipal UserDetails currentUser) {
        User user = (User) userService.loadUserByUsername(currentUser.getUsername());
        model.addAttribute("currentUser", user);
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("allProjects", projectService.allProjects());
        model.addAttribute("allTasks", taskService.allTasks());
        model.addAttribute("allCommentsTask", commentTaskService.allCommentsTask());
        return "admin/admin";
    }

    @PostMapping("/admin")
    public String deleteUser(@RequestParam(required = true, defaultValue = "") Long userId, Model model) {
        userService.deleteUser(userId);
        return "redirect:/admin";
    }
}
