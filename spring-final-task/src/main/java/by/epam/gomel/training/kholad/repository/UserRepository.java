package by.epam.gomel.training.kholad.repository;

import  by.epam.gomel.training.kholad.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
