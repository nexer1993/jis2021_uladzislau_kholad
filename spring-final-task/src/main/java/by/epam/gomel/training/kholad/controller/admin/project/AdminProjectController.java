package by.epam.gomel.training.kholad.controller.admin.project;

import by.epam.gomel.training.kholad.entity.Project;
import by.epam.gomel.training.kholad.entity.User;
import by.epam.gomel.training.kholad.service.ProjectService;
import by.epam.gomel.training.kholad.service.TaskService;
import by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@PropertySource("classpath:status.properties")
public class AdminProjectController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Value(value="${entity.status}")
    private String[] status;

    @GetMapping("/admin/new-project")
    public String createProject(Model model, @AuthenticationPrincipal UserDetails currentUser) {
        User user = (User) userService.loadUserByUsername(currentUser.getUsername());
        model.addAttribute("currentUser", user);
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("projectCreateForm",new Project());
        model.addAttribute("allTasks", taskService.allTasks());
        model.addAttribute("status", status);
        return "admin/project/new-project";
    }

    @PostMapping("/admin/new-project")
    public String addProject(@ModelAttribute("projectCreateForm") @Valid Project projectForm,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("status", status);
            return "admin/project/new-project";
        }
        projectService.saveProject(projectForm);
        return "redirect:/admin";
    }

    @GetMapping("/admin/update-project-{projectId}")
    private String updateProjectForm(@PathVariable("projectId") Long projectId, Model model){
        Project project = projectService.findProjectById(projectId);
        model.addAttribute("projectUpdateForm",project);
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("allTasks", taskService.allTasks());
        model.addAttribute("status", status);
        return "admin/project/update-project";
    }

    @PostMapping("/admin/update-project")
    private String updateProjectDB(@ModelAttribute("projectUpdateForm") @Valid Project projectForm, BindingResult bindingResult,
                                   Model model){

        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("status", status);
            return "admin/project/update-project";
        }
        if(status[2].equals(projectForm.getStatus())){
            projectForm.setUsers(null);
        }
        projectService.saveProject(projectForm);
        return "redirect:/admin";
    }

    @DeleteMapping("/admin/delete-project")
    public String deleteProject(@RequestParam(required = true, defaultValue = "") Long projectID,Model model){
        projectService.deleteProjects(projectID);
        return "redirect:/admin";
    }
}
