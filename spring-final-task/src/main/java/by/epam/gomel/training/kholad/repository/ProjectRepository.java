package by.epam.gomel.training.kholad.repository;

import by.epam.gomel.training.kholad.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ProjectRepository extends JpaRepository<Project, Long> {
}
