package by.epam.gomel.training.kholad.repository;

import  by.epam.gomel.training.kholad.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
