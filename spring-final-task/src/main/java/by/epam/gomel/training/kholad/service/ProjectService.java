package by.epam.gomel.training.kholad.service;

import by.epam.gomel.training.kholad.entity.Project;
import by.epam.gomel.training.kholad.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {
    @Autowired
    ProjectRepository projectRepository;

    public List<Project> allProjects() {
        return projectRepository.findAll();
    }

    public void saveProject(Project project) {
        projectRepository.save(project);
    }

    public void deleteProjects(Long projectId) {
        projectRepository.deleteById(projectId);
    }

    public Project findProjectById(Long projectId) {
        Optional<Project> projectFromDb = projectRepository.findById(projectId);
        return projectFromDb.orElse(new Project());
    }
}
