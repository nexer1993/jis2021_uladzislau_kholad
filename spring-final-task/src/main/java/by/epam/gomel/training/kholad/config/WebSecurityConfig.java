package  by.epam.gomel.training.kholad.config;

import  by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    UserService userService;

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf()
                    .disable()
                .authorizeRequests()
                    //Access only for unregistered users
                    .antMatchers("/registration").not().fullyAuthenticated()
                    //Access only for users with the Administrator role
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .antMatchers("/projects").hasRole("USER")
                    //Access is allowed to all users
                    .antMatchers("/", "/resources/**").permitAll()
                //All other pages require authentication
                .anyRequest().authenticated()
                .and()
                    //Setting up to log in
                    .formLogin()
                    .loginPage("/login")
                    //Redirecting to the projects page after a successful login
                    .defaultSuccessUrl("/")
                    .permitAll()
                .and()
                    .logout()
                    .permitAll()
                    .logoutSuccessUrl("/");
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }
}
