package by.epam.gomel.training.kholad.controller.admin.comment;

import by.epam.gomel.training.kholad.entity.CommentTask;
import by.epam.gomel.training.kholad.service.CommentTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AdminCommentTaskController {
    @Autowired
    private CommentTaskService commentTaskService;

    @DeleteMapping("/admin/delete-comment")
    public String deleteComment(@RequestParam(required = true, defaultValue = "") Long commentID,Model model){
        CommentTask commentTaskById = commentTaskService.findCommentTaskById(commentID);
        commentTaskById.setTask(null);
        commentTaskService.deleteCommentTask(commentTaskById.getId());
        return "redirect:/admin";
    }
}
