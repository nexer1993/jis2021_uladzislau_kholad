package by.epam.gomel.training.kholad.entity;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "t_task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(min = 4, message = "The title should be at least 4 characters long")
    private String title;
    @NotEmpty( message = "Choose date start")
    private String dateStart;

    private String dateEnd;
    private String status;

    @ManyToOne
    @JoinTable(name="t_task_and_user",
            joinColumns = @JoinColumn(name="task_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="user_id", referencedColumnName="id")
    )
    private User userExecutor;

    @ManyToOne
    @JoinTable(name="t_project_and_tasks",
            joinColumns = @JoinColumn(name="task_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="project_id", referencedColumnName="id")
    )
    private Project project;

    @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, fetch = FetchType.EAGER,orphanRemoval = true)
    private Set<CommentTask> comments;


    public Set<CommentTask> getComments() {
        return comments;
    }

    public void setComments(Set<CommentTask> comments) {
        this.comments = comments;
    }
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public  Task(){}

    public User getUserExecutor() {
        return userExecutor;
    }

    public void setUserExecutor(User userExecutor) {
        this.userExecutor = userExecutor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dataStart) {
        this.dateStart = dataStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dataEnd) {
        this.dateEnd = dataEnd;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
