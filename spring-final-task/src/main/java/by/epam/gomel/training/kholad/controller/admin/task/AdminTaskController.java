package by.epam.gomel.training.kholad.controller.admin.task;


import by.epam.gomel.training.kholad.entity.Task;
import by.epam.gomel.training.kholad.service.CommentTaskService;
import by.epam.gomel.training.kholad.service.ProjectService;
import by.epam.gomel.training.kholad.service.TaskService;
import by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Controller
@PropertySource("classpath:status.properties")
public class AdminTaskController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private CommentTaskService commentTaskService;
    @Value(value="${entity.status}")
    private String[] status;

    @GetMapping("/admin/new-task")
    public String createTask(Model model) {
        model.addAttribute("taskCreateForm",new Task());
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("allProjects", projectService.allProjects());
        model.addAttribute("status", status);

        return "admin/task/new-task";
    }

    @PostMapping("/admin/new-task")
    public String addTask(@ModelAttribute("taskCreateForm") @Valid Task taskForm,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("allProjects", projectService.allProjects());
            model.addAttribute("status", status);
            return "admin/task/new-task";
        }
        if(status[2].equals(taskForm.getStatus())){
            taskForm.setDateEnd(String.valueOf(LocalDate.now()));
        }

        taskService.saveTask(taskForm);
        return "redirect:/admin";
    }

    @GetMapping("/admin/update-task-{taskId}")
    private String updateTaskForm(@PathVariable("taskId") Long taskId, Model model){
        Task task = taskService.findTaskById(taskId);
        model.addAttribute("taskUpdateForm",task);
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("allProjects", projectService.allProjects());
        model.addAttribute("allCommentTask", commentTaskService.allCommentsTask());

        model.addAttribute("status", status);
        return "admin/task/update-task";
    }


    @PostMapping("/admin/update-task")
    private String updateTaskDB(@ModelAttribute("taskUpdateForm") @Valid Task taskForm,
                                BindingResult bindingResult,
                                Model model){
        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("allProjects", projectService.allProjects());
            model.addAttribute("status", status);
            return "admin/task/update-task";
        }
        if(status[2].equals(taskForm.getStatus())){
            taskForm.setDateEnd(String.valueOf(LocalDate.now()));
        }
        taskService.saveTask(taskForm);
        return "redirect:/admin";
    }

    @DeleteMapping("/admin/delete-task")
    public String deleteTask(@RequestParam(required = true, defaultValue = "") Long taskID,Model model){
        taskService.deleteTask(taskID);
        return "redirect:/admin";
    }
}
