package by.epam.gomel.training.kholad.service;

import by.epam.gomel.training.kholad.entity.Task;
import by.epam.gomel.training.kholad.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired
    TaskRepository taskRepository;

    public List<Task> allTasks() {
        return taskRepository.findAll();
    }

    public void saveTask(Task task) {
        taskRepository.save(task);
    }

    public void deleteTask(Long taskId) {
        taskRepository.deleteById(taskId);
    }

    public Task findTaskById(Long taskId) {
        Optional<Task> taskFromDb = taskRepository.findById(taskId);
        return taskFromDb.orElse(new Task());
    }
}

