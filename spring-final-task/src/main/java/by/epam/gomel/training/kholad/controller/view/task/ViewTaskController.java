package by.epam.gomel.training.kholad.controller.view.task;

import by.epam.gomel.training.kholad.entity.Project;
import by.epam.gomel.training.kholad.entity.Task;
import by.epam.gomel.training.kholad.entity.CommentTask;
import by.epam.gomel.training.kholad.entity.User;
import by.epam.gomel.training.kholad.service.CommentTaskService;
import by.epam.gomel.training.kholad.service.ProjectService;
import by.epam.gomel.training.kholad.service.TaskService;
import by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;

@Controller
@PropertySource("classpath:status.properties")
public class ViewTaskController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private CommentTaskService commentTaskService;
    @Value(value="${entity.status}")
    private String[] status;

    @GetMapping("/project/task-{taskID}")
    private String showSelectTask(@PathVariable("taskID") Long taskID, Model model, Principal principal) {
        User user = (User) userService.loadUserByUsername(principal.getName());
        Task task = taskService.findTaskById(taskID);
        try {
            boolean  isAccess = checkAccessProjectForUser( task.getProject().getId(), user.getId());
            if (!isAccess) {
                model.addAttribute("message", "You don't have access or there is no such page");
                return "view/info";
            }
        }catch (NullPointerException e){
            model.addAttribute("message", "You don't have access or there is no such page");
            return "view/info";
        }
        model.addAttribute("currentUser", user);
        model.addAttribute("taskInfo", task);
        model.addAttribute("allComments", commentTaskService.allCommentsTask());
        model.addAttribute("commentsTaskForm", new CommentTask());

        return "view/task";
    }

    @PostMapping("/project/task-{taskID}")
    private String addComment(@PathVariable("taskID") Long taskID,
                              @ModelAttribute("commentsTaskForm") @Valid CommentTask commentTask,
                              BindingResult bindingResult, Model model) {
        Task task = taskService.findTaskById(taskID);
        model.addAttribute("allComments", commentTaskService.allCommentsTask());
        model.addAttribute("taskInfo", task);
        if (bindingResult.hasErrors()) {
            return "view/task";
        }

        commentTask.setDate(String.valueOf(LocalDate.now()));
        commentTaskService.saveCommentTask(commentTask);
        return "redirect:/project/task-" + taskID;
    }

    @GetMapping("/project/task-{taskID}/update")
    private String updateTaskPages(@PathVariable("taskID") Long taskID, Model model) {
        Task task = taskService.findTaskById(taskID);
        model.addAttribute("taskUpdateForm", task);
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("allCommentTask", commentTaskService.allCommentsTask());
        model.addAttribute("status", status);
        return "view/update-task";
    }

    @PostMapping("/project/task-{taskID}/update")
    private String updateTaskDB(@ModelAttribute("taskUpdateForm") @Valid Task taskForm,
                                BindingResult bindingResult,
                                Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("status", status);
            return "view/update-task";
        }

        if (status[2].equals(taskForm.getStatus())) {
            taskForm.setDateEnd(String.valueOf(LocalDate.now()));
        }
        taskService.saveTask(taskForm);
        return "redirect:/project-" + taskForm.getProject().getId();
    }

    @GetMapping("/project-{projectID}/new-task")
    private String showNewTask(@PathVariable("projectID") Long projectID, Model model, Principal principal) {
        User user = (User) userService.loadUserByUsername(principal.getName());
        boolean isAccess = checkAccessProjectForUser(projectID, user.getId());
        if (!isAccess) {
            model.addAttribute("message", "You don't have access or there is no such page");
            return "view/info";
        }
        model.addAttribute("status", status);
        model.addAttribute("taskCreateForm", new Task());
        model.addAttribute("allUsers", userService.allUsers());
        model.addAttribute("selectedProject", projectID);
        return "view/new-task";
    }

    @PostMapping("/project-{projectID}/new-task")
    private String createTask(@ModelAttribute("taskCreateForm") @Valid Task taskForm,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("allUsers", userService.allUsers());
            model.addAttribute("status", status);
            return "view/new-task";
        }
        if (status[2].equals(taskForm.getStatus())) {
            taskForm.setDateEnd(String.valueOf(LocalDate.now()));
        }
        taskService.saveTask(taskForm);
        return "redirect:/project-{projectID}";
    }

    private boolean checkAccessProjectForUser(Long projectID, Long userID) {
        Project project = projectService.findProjectById(projectID);
        User user = userService.findUserById(userID);
        for (Project p : user.getProjects()) {
            if (p.getId() == projectID) {
                return true;
            }
        }
        return false;
    }
}
