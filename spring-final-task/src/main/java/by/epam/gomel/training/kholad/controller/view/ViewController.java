package by.epam.gomel.training.kholad.controller.view;

import by.epam.gomel.training.kholad.entity.Project;
import by.epam.gomel.training.kholad.entity.User;
import by.epam.gomel.training.kholad.service.ProjectService;
import by.epam.gomel.training.kholad.service.TaskService;
import by.epam.gomel.training.kholad.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.security.Principal;

@Controller
public class ViewController {
    @Autowired
    private UserService userService;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private TaskService taskService;

    @GetMapping("/project")
    public String projectInfo(Model model, Principal principal) {
        User user = (User) userService.loadUserByUsername(principal.getName());
        model.addAttribute("currentUser", user);
        return "view/project";
    }

    @PostMapping("/project")
    private String showProject(@RequestParam(required = true, defaultValue = "") Long projectID, Model model, Principal principal) {
        User user = (User) userService.loadUserByUsername(principal.getName());
        model.addAttribute("currentUser", user);
        Project project = projectService.findProjectById(projectID);
        model.addAttribute("selectedProject", project);
        return "redirect:/project-" + projectID;
    }

    @GetMapping("/project-{projectID}")
    public String projectInfoAboutUser(@PathVariable("projectID") Long projectID, Model model, Principal principal) {
        User user = (User) userService.loadUserByUsername(principal.getName());
        model.addAttribute("currentUser", user);
        Project project = projectService.findProjectById(projectID);
        if (!checkAccessProjectForUser(projectID, user.getId())) {
            model.addAttribute("message", "You don't have access or there is no such page");
            return "view/info";
        }
        model.addAttribute("selectedProject", project);

        return "view/project";
    }

    private boolean checkAccessProjectForUser(Long projectID, Long userID) {
        Project project = projectService.findProjectById(projectID);
        User user = userService.findUserById(userID);
        for (Project p : user.getProjects()) {
            if (p.getId() == projectID) {
                return true;
            }
        }
        return false;
    }


}
