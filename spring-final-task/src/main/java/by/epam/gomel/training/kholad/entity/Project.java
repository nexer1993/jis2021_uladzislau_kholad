package by.epam.gomel.training.kholad.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(name = "t_project")
public class Project{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 4, message = "The title should be at least 4 characters long")
    private  String title;
    private int idAdminCreated;
    private String status;


    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, fetch = FetchType.EAGER,orphanRemoval = true)
    private Set<Task> tasks;


    @ManyToMany
    @JoinTable(name="t_project_and_users",
            joinColumns = @JoinColumn(name="project_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="user_id", referencedColumnName="id")
    )
    private Set<User> users;

    public Project(){}

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String listIdUsers) {
        this.title = listIdUsers;
    }

    public int getIdAdminCreated() {
        return idAdminCreated;
    }

    public void setIdAdminCreated(int idAdminCreated) {
        this.idAdminCreated = idAdminCreated;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
