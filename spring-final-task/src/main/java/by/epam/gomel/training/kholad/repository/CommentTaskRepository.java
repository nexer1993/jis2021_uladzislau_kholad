package by.epam.gomel.training.kholad.repository;

import by.epam.gomel.training.kholad.entity.CommentTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentTaskRepository extends JpaRepository<CommentTask, Long> {
}
