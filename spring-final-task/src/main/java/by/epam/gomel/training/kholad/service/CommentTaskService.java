package by.epam.gomel.training.kholad.service;

import by.epam.gomel.training.kholad.entity.CommentTask;
import by.epam.gomel.training.kholad.repository.CommentTaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentTaskService {
    @Autowired
    CommentTaskRepository commentTaskRepository;

    public List<CommentTask> allCommentsTask() {
        return commentTaskRepository.findAll();
    }

    public void saveCommentTask(CommentTask taskComment) {
        commentTaskRepository.save(taskComment);
    }

    public void deleteCommentTask(Long commentTaskID) {
        commentTaskRepository.deleteById(commentTaskID);
    }

    public CommentTask findCommentTaskById(Long commentTaskID) {
        Optional<CommentTask> commentTaskFromDb = commentTaskRepository.findById(commentTaskID);
        return commentTaskFromDb.orElse(new CommentTask());
    }
}
