package by.epam.gomel.training.kholad.entity;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = "t_comment_for_task")
public class CommentTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String userID;

    @ManyToOne
    @JoinTable(name="t_comments_task_and_task",
            joinColumns = @JoinColumn(name="task_coment_id", referencedColumnName="id"),
            inverseJoinColumns = @JoinColumn(name="task_id", referencedColumnName="id")
    )
    private Task task;


    @Size(min = 10, message = "The comment should be at least 10 characters long")
    private String comment;
    private String date;

    public CommentTask() {
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
