-- add roles
INSERT INTO public.t_role(id, name) SELECT 1, 'ROLE_USER'
WHERE NOT EXISTS ( SELECT id FROM public.t_role WHERE id = 1 );

INSERT INTO public.t_role(id, name) SELECT 2, 'ROLE_ADMIN'
WHERE NOT EXISTS ( SELECT id FROM public.t_role WHERE id = 2 );


