<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h2>${message}</h2>
    <hr>
    <a href="${pageContext.request.contextPath}/project"><spring:message code="back"/></a><br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>