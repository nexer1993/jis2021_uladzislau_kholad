<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h3><spring:message code="update-task.h3"/></h3>
    <table>
        <thead>
        <th>ID</th>
        <th>ID project</th>
        <th>Title</th>
        <th>Executor</th>
        <th>Status</th>
        <th>Date start</th>
        <th>Date end</th>
        </thead>
        <tr>
            <td>${taskUpdateForm.id}</td>
            <td>${taskUpdateForm.project.id}</td>
            <td>${taskUpdateForm.title}</td>
            <td>${taskUpdateForm.userExecutor.id}</td>
            <td>${taskUpdateForm.status}</td>
            <td>${taskUpdateForm.dateStart}</td>
            <td>${taskUpdateForm.dateEnd}</td>
        </tr>
    </table>
    <br>
    <form:form method="POST"   modelAttribute="taskUpdateForm">
        <form:input type="hidden" path="id" value="${taskUpdateForm.id}"></form:input>
        <form:input type="hidden" path="project" value="${taskUpdateForm.project.id}"></form:input>
        <form:select style="display:none;" multiple="true" path="comments" items="${allCommentTask}" itemLabel="id" itemValue="id" />
        <br>
        <div>
            <form:label path="title" ><spring:message code="title"/></form:label> <br>
            <form:input type="text" path="title" value="${taskUpdateForm.title}"></form:input>
            <form:errors path="title"></form:errors>
        </div>
        <br>
        <div>
            <form:label path="userExecutor" ><spring:message code="task.executor"/></form:label> <br>
            <form:select path="userExecutor">
                <form:option value="" label="--empty--"/>
                <form:options items="${allUsers}" itemLabel="username" itemValue="id" />
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="status" ><spring:message code="status"/> </form:label><br>
            <form:select path="status">
                <c:forEach items="${status}" var="value">
                    <form:option value="${value}" label="${value}"/>
                </c:forEach>
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="dateStart" ><spring:message code="task.date_start"/></form:label> <br>
            <form:input type="date" path="dateStart" value="${taskUpdateForm.dateStart}"></form:input>
            <form:errors path="dateStart"></form:errors>
        </div>
        <br>
        <button type="submit"><spring:message code="update"/></button>
    </form:form><br>
    <hr>
    <a href="${pageContext.request.contextPath}/project/task-${taskUpdateForm.id}/"><spring:message code="back"/></a><br>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>