<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<div>
    <table>
        <thead>
        <th>Last name</th>
        <th>First name</th>
        <th>Middle name</th>
        <th>Email</th>
        <th>Date birth</th>
        <th>User name</th>
        <th>Projects</th>
        </thead>
        <tr>
            <td>${currentUser.lastName}</td>
            <td>${currentUser.firstName}</td>
            <td>${currentUser.middleName}</td>
            <td>${currentUser.email}</td>
            <td>${currentUser.username}</td>
            <td>${currentUser.dateBirth}</td>
            <td><c:forEach items="${currentUser.projects}" var="project">${project.id}; </c:forEach></td>
        </tr>
    </table>
    <br>
    <hr>
    <div class="text-center">
        <span><spring:message code="view.access_text"/></span>
        <form action="/project" method="POST">
            <select name="projectID">
                <c:forEach items="${currentUser.projects}" var="project">
                    <option value="${project.id}">${project.title}-${project.id} </option>
                </c:forEach>
            </select>
            <button type="submit"><spring:message code="view.show"/></button>
        </form>
    </div>
    <br>

    <c:if test="${not empty selectedProject}">
        <h3 class="title-table">${selectedProject.title} &nbsp;&nbsp; ID-${selectedProject.id}</h3>
        <div class="parrent-table">
            <div class="to_do">
                <h3><b><spring:message code="view.to_do"/></b></h3>
                <c:forEach items="${selectedProject.tasks}" var="task">
                    <c:if test="${'to_do'.equals(task.status)}">
                        <b>Id:</b> ${task.id};
                        <b>Title:</b> ${task.title};
                        <b>Executor:</b>
                        <span class="${task.userExecutor.id == currentUser.id ? "text-success" : ""}">${task.userExecutor.username}-${task.userExecutor.id}\</span>
                        <a href="project/task-${task.id}"> -></a>
                        <hr>
                    </c:if>
                </c:forEach>
            </div>
            <div class="doing">
                <h3><b><spring:message code="view.doing"/></b></h3>
                <c:forEach items="${selectedProject.tasks}" var="task">
                    <c:if test="${'doing'.equals(task.status)}">
                        <b>Id:</b> ${task.id};
                        <b>Title:</b> ${task.title};
                        <b>Executors:</b>
                        <span class="${task.userExecutor.id == currentUser.id ? "text-success" : ""}">${task.userExecutor.username}-${task.userExecutor.id}\</span>
                        <a href="project/task-${task.id}"> -></a>
                        <hr>
                    </c:if>
                </c:forEach>
            </div>
            <div class="done">
                <h3><b><spring:message code="view.done"/></b></h3>
                <c:forEach items="${selectedProject.tasks}" var="task">
                    <c:if test="${'done'.equals(task.status)}">
                        <b>Id:</b> ${task.id};
                        <b>Title:</b> ${task.title};
                        <b>Executors:</b>
                        <span class="${task.userExecutor.id == currentUser.id ? "text-success" : ""}">${task.userExecutor.username}-${task.userExecutor.id}\</span>
                        <a href="project/task-${task.id}"> -></a>
                        <hr>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <br>


        <a class="text-center" style="display:block;"
           href="${pageContext.request.contextPath}/project-${selectedProject.id}/new-task"><spring:message code="view.new_task"/> - ${selectedProject.title}</a>
    </c:if>

    <hr>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>


</body>
</html>