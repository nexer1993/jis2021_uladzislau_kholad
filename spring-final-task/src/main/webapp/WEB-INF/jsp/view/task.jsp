<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h2><spring:message code="view.task"/></h2>
    <table>
        <thead>
        <th>ID</th>
        <th>Project ID</th>
        <th>Title</th>
        <th>Executor</th>
        <th>Date start</th>
        <th>Date end</th>
        <th>STATUS</th>
        </thead>
        <tr>
            <td>${taskInfo.id}</td>
            <td>${taskInfo.project.id}</td>
            <td>${taskInfo.title}</td>
            <td>${taskInfo.userExecutor.username}-${taskInfo.userExecutor.id}</td>
            <td>${taskInfo.dateStart}</td>
            <td>${taskInfo.dateEnd}</td>
            <td>${taskInfo.status}</td>
        </tr>
    </table>
    <br>
    <a href="${pageContext.request.contextPath}/project/task-${taskInfo.id}/update"><spring:message code="update"/></a>
    <br>
    <hr>
    <h3><spring:message code="view.comments"/></h3>
    <div class="wrapper">

        <c:forEach items="${taskInfo.comments}" var="comment">
            <div class="wrapper-item">
                ${comment.comment}
                <br>
                Id: <i>${comment.id}</i>&nbsp;&nbsp;<spring:message code="view.comments_author"/> <i>${comment.userID}</i>&nbsp;&nbsp;<spring:message code="view.comments_date"/> <i>${comment.date}</i>
            </div>
        </c:forEach>
    </div>

    <hr>
    <form:form method="POST" modelAttribute="commentsTaskForm">
        <form:input type="hidden" path="task" value="${taskInfo.id}"></form:input>
        <form:input type="hidden" path="userID" value="${currentUser.username}-${currentUser.id}"></form:input>
        <div>
            <form:label path="comment"><spring:message code="view.comments_you_comments"/></form:label> <br>
            <form:textarea path="comment" rows = "10" cols = "40" />
            <form:errors path="comment"></form:errors>
        </div>
        <br>
        <button type="submit"><spring:message code="view.comments_add"/></button>
    </form:form>

    <hr>
    <br>
    <a href="${pageContext.request.contextPath}/project-${taskInfo.project.id}"><spring:message code="back"/></a><br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>