<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h3><spring:message code="admin.new-project.h3"/></h3>

    <form:form method="POST" modelAttribute="projectCreateForm">
        <form:input type="hidden" path="idAdminCreated" value="${currentUser.id}"></form:input>
        <div>
            <form:label path="title"><spring:message code="title"/></form:label> <br>
            <form:input type="text" path="title" placeholder="Title"></form:input>
            <form:errors path="title"></form:errors>
        </div>
        <br>
        <div>
            <form:label path="users"><spring:message code="admin.project.users"/></form:label> <br>
            <form:select path="users">
                <form:option value="" label="--empty--"/>
                <form:options items="${allUsers}" itemLabel="username" itemValue="id"/>
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="status"><spring:message code="status"/></form:label><br>
            <form:select path="status">
                <c:forEach items="${status}" var="value">
                    <form:option value="${value}" label="${value}"/>
                </c:forEach>
            </form:select>
        </div>
        <br>


        <button type="submit"><spring:message code="create"/></button>
    </form:form><br>
    <hr>
    <a href="/admin"><spring:message code="back"/></a>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>