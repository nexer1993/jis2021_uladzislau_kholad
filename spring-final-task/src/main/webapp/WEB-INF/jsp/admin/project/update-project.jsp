<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp" />
<hr>
<div>
    <h3><spring:message code="admin.update-project.h3"/></h3>
    <table>
        <thead>
        <th>ID</th>
        <th>ID admin created:</th>
        <th>Title</th>
        <th>Tasks</th>
        <th>List id users:</th>
        <th>Status</th>
        </thead>
        <tr>
            <td>${projectUpdateForm.id}</td>
            <td>${projectUpdateForm.idAdminCreated}</td>
            <td>${projectUpdateForm.title}</td>
            <td>
                <c:forEach items="${projectUpdateForm.tasks}" var="task">${task.title}; </c:forEach>
            </td>
            <td>
                <c:forEach items="${projectUpdateForm.users}" var="user">${user.id}; </c:forEach>
            </td>
            <td>${projectUpdateForm.status}</td>
        </tr>
    </table>
    <hr>
    <form:form method="POST" action="${pageContext.request.contextPath}/admin/update-project" modelAttribute="projectUpdateForm">
        <form:input type="hidden" path="id" value="${projectUpdateForm.id}"></form:input>
        <form:input type="hidden" path="idAdminCreated" value="${projectUpdateForm.idAdminCreated}"></form:input>
        <form:select style="display:none;" multiple="true" path="tasks" items="${allTasks}" itemLabel="title" itemValue="id" />
        <br>
        <div>
            <form:label path="title" ><spring:message code="title"/></form:label> <br>
            <form:input type="text" path="title" value="${projectUpdateForm.title}"></form:input>
            <form:errors path="title"></form:errors>
        </div><br>
        <div>
            <form:label path="users" ><spring:message code="admin.project.users"/></form:label> <br>
            <form:select path="users">
                <form:option value="" label="--empty--"/>
                <form:options items="${allUsers}" itemLabel="username" itemValue="id" />
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="status" ><spring:message code="status"/> </form:label><br>
            <form:select path="status">
                <c:forEach items="${status}" var="value">
                    <form:option value="${value}" label="${value}"/>
                </c:forEach>
            </form:select>
        </div><br>

        <button type="submit"><spring:message code="update"/></button>
    </form:form><br>
    <hr>
    <a href="${pageContext.request.contextPath}/admin/"><spring:message code="back"/></a>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>