<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h3><spring:message code="admin.all_users"/></h3>
    <table>
        <thead>
        <th>ID</th>
        <th>User name</th>
        <th>First Name</th>
        <th>Last name</th>
        <th>Middle name</th>
        <th>Date birth</th>
        <th>Email</th>
        <th class="second-bg">Tasks ID</th>
        <th class="third-bg">Projects ID</th>
        <th>Roles</th>
        <th>Delete <span class="second-cl">■</span></th>
        </thead>
        <c:forEach items="${allUsers}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>
                <td>${user.middleName}</td>
                <td>${user.dateBirth}</td>
                <td>${user.email}</td>
                <td class="second-bg">
                    <c:forEach items="${user.tasks}" var="task">${task.id}; </c:forEach>
                </td>
                <td class="third-bg">
                    <c:forEach items="${user.projects}" var="project">${project.id}; </c:forEach>
                </td>
                <td>
                    <c:forEach items="${user.roles}" var="role">${role.name}; </c:forEach>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin" method="POST">
                        <input type="hidden" name="userId" value="${user.id}"/>
                        <button type="submit" ${user.id == currentUser.id || user.tasks !=null ? "disabled" : ""}><spring:message code="delete"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <hr>


    <h3><spring:message code="admin.all_projects"/></h3>
    <table>
        <thead>
        <th>ID</th>
        <th>Title</th>
        <th>Admin created</th>
        <th class="first-bg">Tasks ID</th>
        <th class="third-bg">Users ID</th>
        <th>Status</th>
        <th>Update</th>
        <th>Delete<span class="first-cl">■</span></th>
        </thead>
        <c:forEach items="${allProjects}" var="project">
            <tr>
                <td>${project.id}</td>
                <td>${project.title}</td>
                <td>${project.idAdminCreated}</td>
                <td class="first-bg">
                    <c:forEach items="${project.tasks}" var="task">${task.id}; </c:forEach>
                </td>
                <td class="third-bg">
                    <c:forEach items="${project.users}" var="user">${user.id}; </c:forEach>
                </td>
                <td>${project.status}</td>
                <td>
                    <a href="/admin/update-project-${project.id}"><spring:message code="update"/></a>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/delete-project" method="POST">
                        <input type="hidden" name="_method" value="DELETE"/>
                        <input type="hidden" name="projectID" value="${project.id}"/>
                        <c:if test="${empty project.tasks}">
                            <button type="submit" > <spring:message code="delete"/></button>
                        </c:if>
                        <c:if test="${not empty project.tasks}">
                            <button type="submit" disabled> <spring:message code="delete"/></button>
                        </c:if>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <a href="/admin/new-project"><spring:message code="admin.create_project"/></a>
    <hr>


    <h3><spring:message code="admin.all_tasks"/></h3>
    <table>
        <thead>
        <th>ID</th>
        <th class="first-bg">Project ID</th>
        <th>Title</th>
        <th class="second-bg">Executor ID</th>
        <th>Status</th>
        <th>Date start</th>
        <th>Date end</th>
        <th class="fourth-bg">Comments ID</th>
        <th>Update</th>
        <th>Delete <span class="first-cl">■</span> <span class="second-cl">■</span></th>
        </thead>
        <c:forEach items="${allTasks}" var="task">
            <tr>
                <td>${task.id}</td>
                <td class="first-bg">${task.project.id}</td>
                <td>${task.title}</td>
                <td class="second-bg"> ${task.userExecutor.id}</td>
                <td>${task.status}</td>
                <td>${task.dateStart}</td>
                <td>${task.dateEnd}</td>
                <td class="fourth-bg">
                    <c:forEach items="${task.comments}" var="comment">
                        ${comment.id};
                    </c:forEach>
                </td>
                <td>
                    <a href="/admin/update-task-${task.id}"><spring:message code="update"/></a>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/delete-task" method="POST">
                        <input type="hidden" name="_method" value="DELETE"/>
                        <input type="hidden" name="taskID" value="${task.id}"/>
                        <button type="submit" ${task.project !=null || task.userExecutor !=null  ? "disabled" : ""}>
                            <spring:message code="delete"/>
                        </button>
                    </form>
                </td>
            </tr>
        </c:forEach>

    </table>
    <br>
    <a href="/admin/new-task"><spring:message code="admin.create_task"/></a>
    <hr>


    <h3><spring:message code="admin.all_comments"/></h3>
    <table>
        <thead>
        <th>Comment ID</th>
        <th>Text</th>
        <th class="fourth-bg">Task ID</th>
        <th>User</th>
        <th>DELETE</th>
        </thead>

        <c:forEach items="${allCommentsTask}" var="comment">
            <tr>
                <td>${comment.id}</td>
                <td>${comment.comment}</td>
                <td class="fourth-bg">${comment.task.id}</td>
                <td>${comment.userID} </td>
                <td>
                    <form action="${pageContext.request.contextPath}/admin/delete-comment" method="POST">
                        <input type="hidden" name="_method" value="DELETE"/>
                        <input type="hidden" name="commentID" value="${comment.id}"/>
                        <button type="submit" ><spring:message code="delete"/></button>
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>