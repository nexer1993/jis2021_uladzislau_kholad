<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>
<hr>
<div>
    <h3><spring:message code="new-task.h3"/></h3>
    <form:form method="POST" modelAttribute="taskCreateForm">
        <div>
            <form:label path="title" ><spring:message code="title"/> </form:label> <br>
            <form:input type="text" path="title" placeholder="Title"></form:input>
            <form:errors path="title"></form:errors>
        </div>
        <br>
        <div>
            <form:label path="project" ><spring:message code="task.project_id"/></form:label> <br>

            <form:select path="project">
                <form:option value="" label="--empty--"/>
                <form:options items="${allProjects}" itemLabel="id" itemValue="id" />
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="userExecutor" ><spring:message code="task.executor"/></form:label> <br>

            <form:select path="userExecutor">
                <form:option value="" label="--empty--"/>
                <form:options items="${allUsers}" itemLabel="username" itemValue="id" />
            </form:select>
        </div>
        <br>
        <div>
            <form:label path="dateStart" ><spring:message code="task.date_start"/></form:label> <br>
            <form:input type="date" path="dateStart" placeholder="Data start"></form:input>
            <form:errors path="dateStart"></form:errors>
        </div>
        <br>
        <div>
            <form:label path="status" ><spring:message code="status"/> </form:label><br>
            <form:select path="status">
                <c:forEach items="${status}" var="value">
                    <form:option value="${value}" label="${value}"/>
                </c:forEach>
            </form:select>
        </div>
        <br>
        <button type="submit"><spring:message code="create"/></button>
    </form:form><br>
    <hr>
    <a href="/admin"><spring:message code="back"/></a>
    <br>
    <a href="/"><spring:message code="main"/></a>
</div>
</body>
</html>