<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>

<jsp:include page="header.jsp" />

<div>
  <form:form method="POST" modelAttribute="userForm">
    <h2>Registration</h2>
    <div>
      <form:input type="text" path="firstName" placeholder="First name"></form:input>
      <form:errors path="firstName"></form:errors>
    </div><br>
    <div>
      <form:input type="text" path="lastName" placeholder="Last name"></form:input>
      <form:errors path="lastName"></form:errors>
    </div><br>
    <div>
      <form:input type="text" path="middleName" placeholder="Middle name"></form:input>
      <form:errors path="middleName"></form:errors>
    </div><br>
    <div>
      <form:input type="date" path="dateBirth" placeholder="data birth"></form:input>
      <form:errors path="dateBirth"></form:errors>
    </div><br>
    <div>
      <form:input type="text" path="email" placeholder="email" ></form:input>
      <form:errors path="email"></form:errors>
    </div><br>
    <div>
      <div>
        <input type="checkbox" id="is_admin" name="isAdmin" value="admin">
        <label for="is_admin">Admin</label>
      </div>
    </div><br>
    <div>
      <form:input type="text" path="username" placeholder="Username"></form:input>
      <form:errors path="username"></form:errors>
        ${usernameError}
    </div><br>
    <div>
      <form:input type="password" path="password" placeholder="Password"></form:input>
    </div><br>
    <div>
      <form:input type="password" path="passwordConfirm"
                  placeholder="Confirm your password"></form:input>
      <form:errors path="password"></form:errors>
        ${passwordError}
    </div><br>
    <button type="submit">Registration</button>
  </form:form>

  <hr>
  <br>
  <a href="/">Main</a>
</div>
</body>
</html>