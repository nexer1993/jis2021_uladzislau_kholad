<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="header.jsp" />

<div>
  <sec:authorize access="!isAuthenticated()">
    <a href="/login"><spring:message code="index.login"/></a>&nbsp;&nbsp;
    <a href="/registration"><spring:message code="index.registration"/></a>
  </sec:authorize>
  <sec:authorize access="isAuthenticated()">
    <a href="/logout"><spring:message code="index.logout"/></a>
    <a style="position:absolute;top: 0;right: 0;margin: 16px 8px;display:block;" href="/language"><spring:message code="index.language"/></a>
  </sec:authorize>
  <hr>
  <h4><a href="/project"><spring:message code="index.application"/></a></h4>
  <sec:authorize access="hasRole('ADMIN')">
    <h4><a href="/admin"><spring:message code="index.admin_page"/></a></h4>
  </sec:authorize>
  <br>



</div>
</body>
</html>