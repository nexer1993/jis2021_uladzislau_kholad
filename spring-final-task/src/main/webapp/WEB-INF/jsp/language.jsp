<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="${contextPath}/WEB-INF/jsp/header.jsp"/>

<a href="?lang=ru"><spring:message code="language.ru"/></a>&nbsp;&nbsp;
<a href="?lang=en"><spring:message code="language.eng"/></a>
<hr>
<br>
<a href="/"><spring:message code="main"/></a>
</body>
</html>