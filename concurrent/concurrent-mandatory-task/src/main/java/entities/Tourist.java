package entities;

import app.ProgramInSpain;

import java.util.concurrent.*;

import static app.Main.LOG;

public class Tourist implements Runnable {
    private static final Semaphore SEM_MONA_LISA = new Semaphore(5, true);
    private Phaser bus;
    private int numTourist;
    private boolean isInTravel;
    private int countPhoto = 0;
    private int timeEating;

    public int getNumTourist() {
        return numTourist;
    }

    public int getTimeEating() {
        return timeEating;
    }


    public Tourist(Phaser bus, int numTourist) {
        this.bus = bus;
        this.numTourist = numTourist;
        timeEating = randomInt(1, 5);
    }

    public void doPhoto() {
        countPhoto += randomInt(1, 5);
    }

    public int randomInt(int min, int max) {
        int b = (int) (Math.random() * (max - min + 1) + min);
        return b;
    }

    private void wathMonaLisa() {
        try {
            SEM_MONA_LISA.acquire();
            LOG.info("Tourist-" + numTourist + " looks at the Mona Lisa");
            Thread.sleep(100);
        } catch (InterruptedException e) {
            LOG.error(e.getMessage());
        }
        LOG.info("Tourist-" + numTourist + " -> goes to the bus");
        SEM_MONA_LISA.release();
    }

    @Override
    public void run() {
        isInTravel = true;
        LOG.info("Tourist-" + numTourist + " ready to ride a bus");

        bus.arriveAndAwaitAdvance(); //bus start
        bus.arriveAndAwaitAdvance(); //stop in Praga
        doPhoto();

        bus.arriveAndAwaitAdvance(); // going to Paris
        bus.arriveAndAwaitAdvance(); // stop in Paris
        wathMonaLisa();
        doPhoto();

        bus.arriveAndAwaitAdvance(); //going to Rome
        bus.arriveAndAwaitAdvance(); // stop in Rome
        doPhoto();

        bus.arriveAndAwaitAdvance(); // going to  Spain
        bus.arriveAndAwaitAdvance(); // stop in Spain
        doPhoto();

        try {
            int restTime = randomInt(1, 5);
            LOG.info("Tourist-" + numTourist + " rest in the beach " + restTime + " sec");
            Thread.sleep(restTime * 1000);
        } catch (InterruptedException ex) {
            LOG.error(ex.getMessage());
        }

        Exchanger<Integer> exchanger = new Exchanger<>();
        new Thread(new ProgramInSpain(exchanger)).start();
        try {
            exchanger.exchange(countPhoto);
        } catch (InterruptedException ex) {
            LOG.error(ex.getMessage());
        }

        bus.arriveAndAwaitAdvance();// end travel
        isInTravel = false;
    }

}
