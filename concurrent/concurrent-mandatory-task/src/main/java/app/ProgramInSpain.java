package app;

import java.util.concurrent.Exchanger;
import java.util.concurrent.atomic.AtomicInteger;

import static app.Main.LOG;

public class ProgramInSpain implements Runnable{

    private Exchanger<Integer> exchanger;
    public static AtomicInteger quantityPhotoTourists = new AtomicInteger();

    public ProgramInSpain(Exchanger<Integer> ex){
        this.exchanger = ex;
    }

    public static void resetQuantityPhotoTourists(){
        quantityPhotoTourists.set(0);
    }

    public void run(){
        try{
            int expectedValue = exchanger.exchange(0);
            int  newValue  = quantityPhotoTourists.addAndGet(expectedValue);
            quantityPhotoTourists.compareAndSet(expectedValue, newValue);
        } catch(InterruptedException ex){
            LOG.error(ex.getMessage());
        }
    }
}

