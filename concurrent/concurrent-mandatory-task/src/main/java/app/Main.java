package app;

import entities.Tourist;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Phaser;


public class Main {
    private Phaser bus = new Phaser(41);// +main_thread
    private Tourist[] tourists = new Tourist[40];

    public static final Logger LOG = LogManager.getRootLogger();

    public static void main(String[] args) {
        Main mn = new Main();
        mn.start();
    }

    private void start() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 40; j++) { //40
                Tourist tourist = new Tourist(bus, j + 1);
                new Thread(tourist).start();
                tourists[j] = tourist;
            }

            bus.arriveAndAwaitAdvance();
            LOG.info("====>The bus started ");

            bus.arriveAndAwaitAdvance();
            LOG.info("=====The bus stopped in Prague");
            new ProgramInPraga(tourists);

            bus.arriveAndAwaitAdvance();
            LOG.info("====>The bus goes to Paris");

            bus.arriveAndAwaitAdvance();
            LOG.info("=====The bus stopped in Paris");

            bus.arriveAndAwaitAdvance();
            LOG.info("====>The bus goes to Rome");

            bus.arriveAndAwaitAdvance();
            LOG.info("=====The bus stopped in Rome");
            new ProgramInRome(tourists);

            bus.arriveAndAwaitAdvance();
            LOG.info("====>The bus goes to Spain");

            bus.arriveAndAwaitAdvance();
            LOG.info("=====The bus stopped in Spain");

            bus.arriveAndAwaitAdvance();
            LOG.info("||| In the cloud - " + ProgramInSpain.quantityPhotoTourists + " photo");

            LOG.info("=====END TRAVEL");
            LOG.info("=====================================================");
            ProgramInSpain.resetQuantityPhotoTourists();
        }
    }
}
