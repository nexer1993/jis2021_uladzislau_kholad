package app;

import entities.Tourist;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CyclicBarrier;

import static app.Main.LOG;


public class ProgramInRome {

    private BlockingQueue<Tourist> pastaRestaurant = new ArrayBlockingQueue<>(20, true);
    private BlockingQueue<Tourist> pizzaRestaurant = new ArrayBlockingQueue<>(20, true);

    public ProgramInRome(Tourist[] tourists) {
        for (Tourist t : tourists) {
            if (t.getNumTourist() % 2 == 0) {
                pastaRestaurant.add(t);
            } else {
                pizzaRestaurant.add(t);

            }
        }
        restaurant(pastaRestaurant, "pasta");
        restaurant(pizzaRestaurant, "pizza");
    }

    private void restaurant(BlockingQueue<Tourist> queue, String dish) {
        int countPeople = 0;
        int maxTimeEat = 0;

        for (int i = 0; i < 20; i++) {
            try {
                Tourist tourist = queue.take();
                int k = tourist.getTimeEating();
                if (k > maxTimeEat) {
                    maxTimeEat = k;
                }
                LOG.info("Tourist-" + tourist.getNumTourist() + " go eat " + dish);
                if ((i + 1) % 5 == 0) {
                    LOG.info("5 tourist ate " + dish);
                    Thread.sleep(maxTimeEat * 1000);
                    maxTimeEat = 0;
                }
            } catch (InterruptedException ex) {
                LOG.error(ex.getMessage());
            }
        }
    }
}
