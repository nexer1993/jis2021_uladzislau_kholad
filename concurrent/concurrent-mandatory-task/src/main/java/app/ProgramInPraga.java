package app;

import entities.Tourist;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import static app.Main.LOG;

public class ProgramInPraga {
    private Queue<Tourist> queue = null;
    private volatile boolean cycle = true;
    private Tourist[] tourists = null;

    public ProgramInPraga(Tourist[] tourists) {
        this.tourists = tourists;
        queue = new ConcurrentLinkedQueue<Tourist>();
        Thread producer = new Thread(new Producer());
        Thread consumer = new Thread(new Consumer());
        producer.start();
        consumer.start();

        while (consumer.isAlive()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    class Producer implements Runnable {
        public void run() {
            try {
                for (int i = 0; i < tourists.length; i++) {
                    queue.add(tourists[i]);
                    LOG.info("Tourist-" + tourists[i].getNumTourist() + " got in line (producer)");
                }
                cycle = false;
            } catch (Exception ex) {
                LOG.error(ex.getMessage());
            }
        }
    }

    class Consumer implements Runnable {
        public void run() {
            Tourist tourist;
            while (cycle || queue.size() > 0) {
                int timeDrink = 0;
                if ((tourist = queue.poll()) != null) {
                    timeDrink = tourist.getTimeEating();
                    try {
                        Thread.sleep(tourist.getTimeEating());
                    } catch (Exception ex) {
                        LOG.error(ex.getMessage());
                    }
                    LOG.info("Tourist-" + tourist.getNumTourist() + " drinks bear " + tourist.getTimeEating() + " sec (consumer)");
                }
                try {
                    //Thread.sleep(1000 * timeDrink); // real time for drink bear
                    Thread.sleep(100);
                } catch (Exception ex) {
                    LOG.error(ex.getMessage());
                }
            }
        }
    }

}
