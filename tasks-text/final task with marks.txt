Задание для финального проекта.

Предметная область: приложение управления проектами (пример, Trello)

Общее:
- Java 8; +
- WebApp Server: Apache Tomcat 9; +
- Docker; =====
- Spring MVC приложение; +
- Для описания Spring-сущностей использовать Java и аннотационный подходы; +
- Разделение приложения на слои: модель бизнес-логики, слой DAO, слой сервисов, слой контроллеров; +
- База данных: H2 (in-memory или disk-based); +
- Аутентификация и права доступа на основе Spring Security; (+);  +
- Слой отображения: JSP (базовое требование) или шаблонизаторы (+) Thymeleaf, Free-marker; 
- Unit-тестирование бизнес-модели; =====
- Разработать иерархию исключений; =====
- Логгирование всех контроллер-событий; =====
- Интернационализация с помощью Spring: английский и русский языки; =====

Функционал:
- Наличие двух ролей: администратор и обычный пользователь; +
- Вход в приложение только для зарегистрированного пользователя; +
- Информация для регистрации: ФИО, дата рождения, логин, пароль, адрес электронной почты и роль; +
- Пользователю может быть добавлен аватар (+);    =====
- Каждый пользователь имеет личную страницу, на которой выводится личная информация, список проектов и список заданий; +

- Роль администратора:
* Создание/модификация/удаление/окончание проекта и добавление в него пользователей; +
* Создание/модификация/удаление заданий в рамках проекта и изменение их статусов; +
* Доступ к списку проектов со списком заданий; +
* Весь функционал обычного пользователя; +

- Роль пользователя:
* Создание/изменение статуса задания в рамках проекта, в который есть доступ; +
* Доступ может быть к нескольким проектам (выбор в выпадающем списке);  +
* Пользователь должен видеть доску (таблицу) проекта с расставленными на ней заданиями в зависимости от статусов, Drag-n-Drop реализовывать не нужно (+);  +кромеDrag-n-Drop
* На доску выводится только самая важная информация о задании: ID, название, исполнитель; +
* Детальная информация о задании должна выводиться в отдельном окне, там же изменение статуса задания; +
* Пользователь может назначить задание на любого иного пользователя из этого проекта; +
* Пользователь может добавлять комментарий к заданию; +

- Проект состоит из:
* ID; +
* Спикок заданий; +
* Список пользователей; +
* Идентификатор администратора, создавшего проект; +
* Статус проекта: к исполнению / в работе / завершен;  +

- Задание состоит из:
* ID; +
* ID проекта; +
* Название; +
* Исполнитель; +
* Дата начала; +
* Дата окончания, добавляется автоматически при изменении статуса "Завершен"; +
* Статус; +
* Список комментариев к заданию;  +

- Окончание проекта ведет к удалению его из списка проектов, доступных пользователям; +
- Статусы задания должны быть описаны с property-файле в виде массива; +
- Конфигурация подключения к БД описывается в property-файле; +
- SQL-запросы должны быть вынесены в отдельный класс, либо в файл; +
- Все литералы выносятся в файл констант;
- Упор делать не на UI, а на качестве Java-кода; +
- Выполнение всех пунктов задания опционально, при сдаче проекта прикрепить данный файл с отметкой выполненных пунктов;