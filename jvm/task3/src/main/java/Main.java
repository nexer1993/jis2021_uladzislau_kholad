import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        // 1 minute
        // Quantity string with deduplicate 76525
        // Quantity string with intern 18435
        List<String> list = new ArrayList<>();
        while (true) {
            list.add(new String("VALUE").intern());
            Thread.sleep(1);
        }
    }
}
