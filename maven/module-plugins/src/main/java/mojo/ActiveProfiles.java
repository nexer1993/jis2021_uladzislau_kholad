package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name = "activeProfiles")
public class ActiveProfiles extends AbstractMojo {
    @Parameter(property = "myProfile")
    String myProfile;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        System.out.println("|Active profile|");
        System.out.printf("Active profile: %s\n",myProfile);
    }
}
