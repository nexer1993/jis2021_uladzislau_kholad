package mojo;


import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

@Mojo(name = "writerInfoBuildMojo")
public class WriterInfoBuildMojo extends AbstractMojo {
    @Parameter(property = "groupId")
    private String groupId;

    @Parameter(property = "artifactId")
    private String artifactId;

    @Parameter(property = "version")
    private String version;

    @Parameter(property = "build")
    private String build;

    @Parameter(property = "timestamp")
    private String timestamp;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        String filePath = "D:\\nexer\\java\\epam-lab\\files\\info-building.txt";

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8))) {
            String resStr = String.format("Group id: %s \n"
                + "Artifact id: %s \n"
                + "Version: %s \n"
                + "Build: %s \n"
                + "Timestamp: %s \n", groupId, artifactId, version, build, timestamp);
            writer.write(resStr);
            System.out.println("|SUCCESS WRITE IN THE FILE|");
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }
}
