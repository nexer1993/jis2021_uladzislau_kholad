package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static mojo.Sender.sendFromGMail;

@Mojo(name = "sendGmailInfoBuildMojo")
public class SendGmailInfoBuildMojo extends AbstractMojo {
    @Parameter(property = "projectName", defaultValue = "demo-module")
    private String projectName;

    @Parameter(property = "userName")
    private String userName;

    @Parameter(property = "userPassword", defaultValue = "demo-module")
    private String userPassword;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Path path = Paths.get("D:\\nexer\\java\\epam-lab\\files\\info-building.txt");
        List<String> linesPath = new ArrayList<>();
        String body;

        try (Stream<String> lineStream = Files.lines(path)) {
            linesPath = lineStream.collect(Collectors.toList());
            body = String.join(" \n", linesPath);
        } catch (IOException ex) {
            System.err.println(ex);
            body = "The report is missed";
        }

        String[] to = { userName }; // list of recipient email addresses
        String subject =  String.format("%s: installed successfully",projectName);

        sendFromGMail(userName, userPassword, to, subject, body);
        System.out.println("|SEND SUCCESS|");

    }
}
