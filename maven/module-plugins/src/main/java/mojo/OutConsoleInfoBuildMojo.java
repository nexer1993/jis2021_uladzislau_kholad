package mojo;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Mojo(name = "outConsoleInfoBuildMojo")
public class OutConsoleInfoBuildMojo extends AbstractMojo {
    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        List<String> lines = new ArrayList<>();
        Path path = Paths.get("D:\\nexer\\java\\epam-lab\\files\\info-building.txt");

        try (Stream<String> lineStream = Files.lines(path)) {
            lines = lineStream.collect(Collectors.toList());
        } catch (IOException ex) {
            System.err.println(ex);
        }

        System.out.println("|ABOUT BUILDING|");
        String joinedStr = String.join("\n", lines);
        System.out.println(joinedStr);
    }
}
