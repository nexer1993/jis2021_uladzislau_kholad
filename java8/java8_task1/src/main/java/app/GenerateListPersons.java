package app;

import entities.Person;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GenerateListPersons {
    private List<Person> listPerson = new ArrayList<>();

    public List<Person> generateList(int quantityPerson) {
        for (int i = 0; i < quantityPerson; i++) {
            int age = randomInt(14, 110);
            int height = randomInt(85, 220);
            int weight = randomInt(45, 140);
            String firstName = generateRandomText(randomInt(3, 9));
            String lastName = generateRandomText(randomInt(3, 9));
            listPerson.add(new Person(age, height, weight, firstName, lastName));
        }
        return listPerson;
    }

    private int randomInt(int min, int max) {
        int b = (int) (Math.random() * (max - min + 1) + min);
        return b;
    }

    public static String generateRandomText(int len) {
        String chars = "aywutsronmigfedcbah";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }
}
