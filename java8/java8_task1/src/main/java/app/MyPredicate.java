package app;
@FunctionalInterface
public interface MyPredicate<T> {
    boolean test(T t);

    default String testDefault(T t){
        String res = t.toString();
        res = res.strip().toLowerCase();
        return "TestDefault " + res;
    }

    static String staticDelNum(String t){
        String res = t.toString();
        res = res.replaceAll("[0-9]", "");
        return "TestStatic " + res;
    }

}
