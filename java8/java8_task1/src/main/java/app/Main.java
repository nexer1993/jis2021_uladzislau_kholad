package app;

import entities.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.function.*;

public class Main {
    public static final Logger LOG = LogManager.getRootLogger();

    public static void main(String[] args) {
        Main mn = new Main();
        mn.startComparator();
        mn.startFunctionalInterface();
        mn.startMyFunctionalInterface();
    }

    public void startComparator() {
        GenerateListPersons genListPersons = new GenerateListPersons();
        List<Person> listPerson = genListPersons.generateList(10);

        LOG.info("|||  Sort List of person objects by FirstName  |||");
        Comparator<Person> sortByAge = Comparator.comparing(p -> p.getFirstName());
        listPerson.sort(sortByAge);
        listPerson.forEach(p -> LOG.info(p));

        LOG.info("|||  Sort list of person objects by Age then by Height  |||");
        Comparator<Person> sortByAgeByHeight = Comparator.comparing((Person p) -> p.getAge())
                .thenComparing(p -> p.getHeight());
        listPerson.sort(sortByAgeByHeight);
        listPerson.forEach(p -> LOG.info(p));

    }

    public void startFunctionalInterface() {
        separatorLog("Functional Interface");
        Runnable runnable = () -> {
            LOG.info("Runnable : Hello from runnable");
        };
        new Thread(runnable).start();

        Predicate<Integer> predic = x -> (x > 1);
        LOG.info("Predicate 5>1: " + predic.test(5));

        BinaryOperator<Integer> binOper = (x, y) -> x + y;
        LOG.info("BinaryOperator 3+5: " + binOper.apply(3, 5));

        UnaryOperator<String> unarOper = x -> "Hello " + x;
        LOG.info("UnaryOperator Hello + world: " + unarOper.apply("World"));

        Function<String, Integer> func = x -> x.length();
        LOG.info("Function String lenght(w14fsf124): " + func.apply("w14fsf124"));

        Supplier<String> supplier = () -> "Hello from supplier";
        LOG.info("Supplier get : " + supplier.get());
    }

    private void startMyFunctionalInterface() {
        MyPredicate<String> test1 = (x) -> x.length() < 4;
        MyPredicate<String> test2 = (x) -> "111".equals(x);
        MyPredicate<String> test3 = (x) -> x.startsWith("1");

        separatorLog("My Predicate");
        LOG.info("MyPredicate 12345.length() < 4: " + test1.test("12345"));
        LOG.info("MyPredicate 111.equals(222): " + test2.test("222"));
        LOG.info("MyPredicate 123 startsWith 1: " + test3.test("123"));
        LOG.info("Default method(delete space and do lowercase) :" + test1.testDefault(" SA SADfasf@FaSAF     "));
        LOG.info("Static method(delete number) :" + MyPredicate.staticDelNum("sdsda sf14 12412455"));
    }

    private static void separatorLog(String message) {

        LOG.info("============================== " + message + " =================================");
    }
}
