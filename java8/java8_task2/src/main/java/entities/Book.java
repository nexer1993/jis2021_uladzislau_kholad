package entities;

import java.util.List;
import java.util.Set;

public class Book    {
    private String title;
    private int countPages;
    private Set<Author> authors;

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Author s : authors) {
            stringBuilder.append(s.getName());
            stringBuilder.append(". ");
        }

        String res = String.format("Book { Title:%s || Pages:%d || List authors: %s}", title,countPages,stringBuilder);
        return res;
    }

    public String getTitle() {
        return title;
    }

    public int getCountPages() {
        return countPages;
    }

    public Set<Author> getListAuthor() {
        return authors;
    }

    public void setListAuthor(Set<Author> authors) {
        this.authors = authors;
    }

    public Book(String title, int countPages) {
        this.title = title;
        this.countPages = countPages;
    }

}
