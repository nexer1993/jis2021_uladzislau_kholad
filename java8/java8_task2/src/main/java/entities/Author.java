package entities;

import java.util.List;
import java.util.Set;

public class Author {
    private String name;
    private int age;
    private Set<Book> books ;

    public Author(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Set<Book> getListBooks() {
        return books;
    }

    public void setListBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Book s : books) {
            stringBuilder.append(s.getTitle());
            stringBuilder.append(". ");
        }

        String res = String.format("Author { Name:%s || Age:%d || List books: %s}", name,age,stringBuilder);
        return res;
    }





}
