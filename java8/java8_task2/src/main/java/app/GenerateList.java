package app;

import entities.Author;
import entities.Book;


import java.util.ArrayList;
import java.util.List;


import static app.RandomInt.randomInt;

public class GenerateList {
    public  List generateList(int quantityObj, String typeList) {
        ReadFileResources  readFileResources = new ReadFileResources();
        List resultList = new ArrayList<>();

        List<String> linesBooks = readFileResources.loadFile("listBooks");
        List<String> linesAuthors = readFileResources.loadFile("listAuthors");

        if(linesBooks.size() < quantityObj){
            quantityObj = linesBooks.size();
        }
        if(linesAuthors.size() < quantityObj){
            quantityObj = linesAuthors.size();
        }

        if("book".equals(typeList)){
            for (int i = 0; i < quantityObj; i++) {
                resultList.add(new Book(linesBooks.get(i),randomInt(15,500)));
            }
        }else if("author".equals(typeList)){
            for (int i = 0; i < quantityObj; i++) {
                resultList.add(new Author(linesAuthors.get(i),randomInt(14,110)));
            }
        }
        return resultList;
    }


}
