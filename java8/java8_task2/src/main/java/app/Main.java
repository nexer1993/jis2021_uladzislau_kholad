package app;

import entities.Author;
import entities.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {
    public static final Logger LOG = LogManager.getRootLogger();

    public static void main(String[] args) {
        Main mn = new Main();
        mn.start();
    }

    public static void start() {
        GenerateList genList = new GenerateList();
        GenerateLinks genLinks = new GenerateLinks();

        List<Book> listBooks = genList.generateList(20, "book");
        List<Author> listAuthors = genList.generateList(20, "author");

        genLinks.genetareLinks(listBooks, listAuthors);

        LOG.info("=== List books ===");
        listBooks.stream().sorted(Comparator.comparing(Book::getCountPages)).forEach(x -> LOG.info(x));

        LOG.info("=== List authors ===");
        listAuthors.stream().forEach(x -> LOG.info(x));

        LOG.info("=== Books with page more than 200. Task(2.1)  === ");
        listBooks.stream().filter(x -> x.getCountPages() > 200).forEach(x -> LOG.info(x));

        LOG.info("=== All books have more than 200 pages? Task(2.2) === ");
        LOG.info(listBooks.stream().allMatch(x -> x.getCountPages() > 200));

        LOG.info("=== A large number of pages has a book. Task(2.3) === ");
        LOG.info(" --> MAX");
        LOG.info(listBooks.stream().max(Comparator.comparing(Book::getCountPages)).get());
        LOG.info(" --> MIN");
        LOG.info(listBooks.stream().min(Comparator.comparing(Book::getCountPages)).get());

        LOG.info("=== Books with one author. Task(2.4) === ");
        // code there
        LOG.info(" --> Books with only one author");
        listBooks.stream().filter(x -> x.getListAuthor().size() == 1).forEach(x -> LOG.info(x));

        LOG.info(" --> All books by the same author (in the list Authors)");
        Map<String, List<String>> booksAnAuthor = listAuthors.stream()
                .collect(Collectors.toMap(Author::getName, books -> books.getListBooks().stream().map(book -> book.getTitle()).collect(Collectors.toList())));

        List<String> booksWithOneAuthors = booksAnAuthor.entrySet().stream()
                .map(Map.Entry::toString)
                .collect(Collectors.toList());

        booksWithOneAuthors.stream().forEach(x -> LOG.info(x));

        LOG.info("===  Sorting by number of pages and title. Task(2.5) ===");
        listBooks.stream().sorted(Comparator.comparing(Book::getCountPages).thenComparing(Book::getTitle)).forEach(x -> LOG.info(x));

        LOG.info("===  Distcint title. Task(2.6) === ");
        listBooks.stream().map(Book::getTitle).distinct().forEach(x -> LOG.info(x));

        LOG.info("===  Distcint list authors with quantity page is less than 200. Task(2.7) ===");
        List<Author> collect = listAuthors.stream()
                .distinct()
                .filter(x -> x.getListBooks().stream()
                        .allMatch(y -> y.getCountPages() < 200))
                .collect(Collectors.toList());

        collect.stream().forEach(x -> LOG.info(x));
    }
}
