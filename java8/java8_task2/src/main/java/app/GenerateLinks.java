package app;

import entities.Author;
import entities.Book;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static app.RandomInt.randomInt;

public class GenerateLinks {
    public void genetareLinks(List<Book> bookList, List<Author> authorList){
        for (int i = 0; i < bookList.size(); i++) {
            Set<Author> setAuthors = new HashSet<>();
            int quantityAuthors = randomInt(1,3);
            if(authorList.size() > quantityAuthors){
                for (int j = 0; j < quantityAuthors; j++) {
                    setAuthors.add(authorList.get(randomInt(1,authorList.size()-1)));
                }
            }
            bookList.get(i).setListAuthor(setAuthors);
        }

        for (int i = 0; i < authorList.size(); i++) {
            Set<Book> setBooks = new HashSet<>();
            int quantityBooks = randomInt(1,5);
            if(bookList.size() > quantityBooks){
                for (int j = 0; j < quantityBooks; j++) {
                    setBooks.add(bookList.get(randomInt(1,9)));//bookList.size()-1
                }
            }
            authorList.get(i).setListBooks(setBooks);
        }
    }
}
