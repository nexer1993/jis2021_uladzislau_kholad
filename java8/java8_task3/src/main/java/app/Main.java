package app;

import entities.Thermometer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Main {
    public static final Logger LOG = LogManager.getRootLogger();

    public static void main(String[] args) {
        List<Thermometer> thermometers = new ArrayList<>();
        generateList(thermometers);
        Map<Integer, List<Thermometer>> sortThermometers = thermometers.stream()
                .collect(Collectors.groupingBy(x -> x.getDate().getMonthValue()));

        Thermometer max = thermometers.stream().max(Comparator.comparing(Thermometer::getTemperature)).get();
        Thermometer min = thermometers.stream().min(Comparator.comparing(Thermometer::getTemperature)).get();
        LOG.info("=== Max and Min temperature in a year. (Task 3.1) === ");
        LOG.info("Max temperature " + max.getTemperature() + " in " + max.getDate());
        LOG.info("Min temperature " + min.getTemperature() + " in " + min.getDate());

        IntSummaryStatistics iStats = thermometers.stream().mapToInt(Thermometer::getTemperature).summaryStatistics();

        LOG.info("=== Average value in a year. (Task 3.2) === ");
        LOG.info("Average value " + iStats.getAverage());


        LOG.info("=== Min and Max value in ever month. (Task 3.3) === ");

        sortThermometers.forEach((k, v) -> LOG.info((
                 "Month " + k +". Temperature Min: " + v.stream().min(Comparator.comparing(Thermometer::getTemperature)).get().getTemperature() +
                  " Max: " + v.stream().max(Comparator.comparing(Thermometer::getTemperature)).get().getTemperature()
        )));

        LOG.info("=== Average value in ever month. (Task 3.4) === ");
        sortThermometers.forEach((k, v) -> LOG.info((
            "Month " + k +". Average temperature in month: " + v.stream().mapToInt(Thermometer::getTemperature).summaryStatistics().getAverage()
        )));


        LOG.info("=== Sorted data list with max value temperature (Task 4) === ");

        List<Thermometer> maxThempInMonth = sortThermometers.entrySet().parallelStream()
                .map(x -> x.getValue().parallelStream().max(Comparator.comparing(Thermometer::getTemperature)).get())
                .sorted(Comparator.comparing(x -> x.getDate()))
                .collect(Collectors.toList());
        LOG.info(" --> Parrallel stream ");
        maxThempInMonth.parallelStream().forEach(x-> LOG.info("Data: " + x.getDate() + " Max temperature: " + x.getTemperature()));
        LOG.info(" --> Stream ");
        maxThempInMonth.stream().forEach(x-> LOG.info(x));
    }

    private static void generateList(List<Thermometer> thermometers){
        LocalDate dataNow = LocalDate.now();
        for (int i = 0; i < dataNow.lengthOfYear(); i++) {
            thermometers.add(new Thermometer(i,LocalDate.ofYearDay(dataNow.getYear(), i+1)));
        }
    }
}
