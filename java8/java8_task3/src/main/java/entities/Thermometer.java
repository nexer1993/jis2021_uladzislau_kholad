package entities;

import java.time.LocalDate;

public class Thermometer{
    private int temperature;
    private LocalDate date;

    public int getTemperature() {
        return temperature;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Thermometer{" +
                "temperature=" + temperature +
                ", date=" + date +
                '}';
    }

    public Thermometer(int temperature, LocalDate date) {
        this.temperature = temperature;
        this.date = date;
    }
}
