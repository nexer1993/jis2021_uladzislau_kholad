package app;

import entites.TestObject;

public class Main {
    public static void main(String[] args) {
        TestObject testObject = new TestObject("First", "Second");
        new ThisCodeSmellsAnnotationHandler().handle(testObject);
    }
}
