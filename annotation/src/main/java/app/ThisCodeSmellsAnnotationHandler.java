package app;

import entites.ThisCodeSmells;
import entites.ThisCodeSmellsCont;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;

public class ThisCodeSmellsAnnotationHandler {
    private List<String> entitesWitsAnnotation = new ArrayList<>();
    private List<String> allReviewers = new ArrayList<>();

    public void handle(Object obj) {
        Class<?> windowsClass = obj.getClass();
        addNameClassWithAnnotate(windowsClass);
        addNameConstructorWithAnnotate(windowsClass);
        addNameFieldsWithAnnotate(windowsClass);
        addNameMethodsWithAnnotate(windowsClass);

        entitesWitsAnnotation.forEach(System.out::println);
        showMostPopularReviewer();
    }

    private void addNameClassWithAnnotate(final Class<?> type) {
        if(type.isAnnotationPresent(ThisCodeSmells.class) || type.isAnnotationPresent(ThisCodeSmellsCont.class)){
            ThisCodeSmells[] annotationsByType = type.getAnnotationsByType(ThisCodeSmells.class);
            Arrays.stream(annotationsByType).forEach(x -> allReviewers.add(x.reviewer()));
            entitesWitsAnnotation.add("Class " + type.getName() + " has annotate @ThisCodeSmells");
        }
    }

    private void addNameFieldsWithAnnotate(final Class<?> type) {
        Field[] declaredFields = type.getDeclaredFields();
        for (Field field : declaredFields) {
            if(field.isAnnotationPresent(ThisCodeSmells.class) || field.isAnnotationPresent(ThisCodeSmellsCont.class)){
                ThisCodeSmells[] annotationsByType = field.getAnnotationsByType(ThisCodeSmells.class);
                Arrays.stream(annotationsByType).forEach(x -> allReviewers.add(x.reviewer()));
                entitesWitsAnnotation.add("Field " + field.getName() + " has annotate @ThisCodeSmells");
            }
        }
    }

    private void addNameConstructorWithAnnotate(final Class<?> type) {
        Constructor<?>[] constructors = type.getDeclaredConstructors();
        for (Constructor constructor : constructors) {
            addParametersWithAnnotation(constructor);

            if(constructor.isAnnotationPresent(ThisCodeSmells.class) || constructor.isAnnotationPresent(ThisCodeSmellsCont.class)){
                ThisCodeSmells[] annotationsByType = constructor.getAnnotationsByType(ThisCodeSmells.class);
                Arrays.stream(annotationsByType).forEach(x -> allReviewers.add(x.reviewer()));
                entitesWitsAnnotation.add("Constructor " + constructor.getName() + " has annotate @ThisCodeSmells");
            }
        }
    }

    private void addNameMethodsWithAnnotate(final Class<?> type) {
        Method[] declaredMethods = type.getDeclaredMethods();
        for (Method method : declaredMethods) {
            addParametersWithAnnotation(method);

            if(method.isAnnotationPresent(ThisCodeSmells.class) || method.isAnnotationPresent(ThisCodeSmellsCont.class)){
                ThisCodeSmells[] annotationsByType = method.getAnnotationsByType(ThisCodeSmells.class);
                Arrays.stream(annotationsByType).forEach(x -> allReviewers.add(x.reviewer()));
                entitesWitsAnnotation.add("Method " + method.getName() + " has annotate @ThisCodeSmells");
            }
        }
    }

    private void addParametersWithAnnotation(Executable executable){
        final Annotation[][] paramAnnotations = executable.getParameterAnnotations();
        for (int i = 0; i < paramAnnotations.length; i++) {
            for (Annotation a : paramAnnotations[i]) {
                if (a instanceof ThisCodeSmells) {
                    String reviewer = ((ThisCodeSmells) a).reviewer();
                    allReviewers.add(reviewer);
                    entitesWitsAnnotation.add( "Parameter " + i + "  in "+ executable.getName() + " with annotate @ThisCodeSmells");
                }
                if (a instanceof ThisCodeSmellsCont) {
                    ThisCodeSmells[] arrayThisCodeSmells = ((ThisCodeSmellsCont) a).value();
                    Arrays.stream(arrayThisCodeSmells).forEach(x -> allReviewers.add(x.reviewer()));
                    entitesWitsAnnotation.add( "Parameter " + i + "  in "+ executable.getName() + " with several annotates @ThisCodeSmells");
                }
            }
        }
    }

    private void showMostPopularReviewer() {
        Map<String, Long> maplistString = allReviewers.stream()
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));

        String key = maplistString.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
        Long maxValue = Collections.max(maplistString.values());
        System.out.println("============================================");
        System.out.println(key + " left a lot of annotations - " + maxValue);
        System.out.println("============================================");
        maplistString.forEach((k, v) -> System.out.println(k + " : " + v));
    }
}
