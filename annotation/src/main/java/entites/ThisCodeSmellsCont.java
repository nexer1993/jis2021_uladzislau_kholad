package entites;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface ThisCodeSmellsCont {
    ThisCodeSmells[] value();
}
