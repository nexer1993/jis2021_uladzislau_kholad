package entites;

import java.lang.annotation.*;

@Retention(value = RetentionPolicy.RUNTIME)
@Repeatable(ThisCodeSmellsCont.class)
public @interface ThisCodeSmells {
    String reviewer() default "Goro";
}


