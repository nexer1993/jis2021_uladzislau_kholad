package entites;

@ThisCodeSmells(reviewer = "Sonya")
public class TestObject {
    @ThisCodeSmells(reviewer = "Raiden")
    public String publicField;

    @ThisCodeSmells(reviewer = "Jax")
    @ThisCodeSmells(reviewer = "Kun Lao")
    private String privateField;

    @ThisCodeSmells(reviewer = "Kun Lao")
    private TestObject(String publicField) {
        this.publicField = publicField;
    }

    @ThisCodeSmells(reviewer = "Jax")
    public TestObject(@ThisCodeSmells String publicField, @ThisCodeSmells @ThisCodeSmells(reviewer = "Sub Zero")  String privateField) {
        this.publicField = publicField;
        this.privateField = privateField;
    }

    @ThisCodeSmells(reviewer = "Sonya")
    @ThisCodeSmells(reviewer = "Kun Lao")
    public void publicMethod() {
    }

    @ThisCodeSmells(reviewer = "Kun Lao")
    private void privateMethod() {
    }

    private void testMethod(int o, @ThisCodeSmells(reviewer = "Raiden") String st) {
    }
}
